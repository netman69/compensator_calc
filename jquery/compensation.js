/* "color theme" */
var col_grat_light = "rgba(0, 0, 0, 0.2)";
var col_grat_dark = "rgba(0, 0, 0, 0.5)";
var col_grat_border = "rgba(0, 0, 0, 0.9)";
var col_grat_text = "rgba(0, 0, 0, 0.8)";
var col_grat_title = "rgba(0, 0, 0, 0.8)";
var col_fres = "rgba(255, 0, 0, 0.5)";
var col_fesr = "rgba(0, 0, 255, 0.5)";
var col_fc = "rgba(200, 0, 255, 0.8)";
var col_pmargin = "rgba(255, 0, 255, 0.5)";
var col_filter = "rgba(200, 0, 0, 1)";
var col_comp = "rgba(200, 0, 200, 1)";
var col_total = "rgba(0, 200, 0, 1)";

/* complex arithmetic helpers */

function cadd(a, b) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	if (typeof b != 'object') b = { re: b, im: 0 };
	return { re: a.re + b.re, im: a.im + b.im };
}

function csub(a, b) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	if (typeof b != 'object') b = { re: b, im: 0 };
	return { re: a.re - b.re, im: a.im - b.im };
}

function crec(a) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	return { re: a.re / (a.re * a.re + a.im * a.im), im: - a.im / (a.re * a.re + a.im * a.im) };
}

function cmul(a, b) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	if (typeof b != 'object') b = { re: b, im: 0 };
	return { re: a.re * b.re - a.im * b.im, im: a.re * b.im + a.im * b.re };
}

function cdiv(a, b) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	if (typeof b != 'object') b = { re: b, im: 0 };
	return cmul(a, crec(b));
}

function cmag(a) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	return Math.sqrt(a.re * a.re + a.im * a.im);
}

function cphase(a) {
	if (typeof a != 'object') a = { re: a, im: 0 };
	return Math.atan2(a.im, a.re);
}

/* this should already be part of js imho */

Math.log10 = function(x) {
	return Math.log(x) / Math.log(10);
};

/* let's get to work */

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");

var frange = 3e6; // frequency range in hz
var grange = 200; // gain range in db
var gstart = grange / 2;
var gstep = 20;
var prange = 240; // phase range in degrees
var pstart = 60;
var pstep = 30;

function grat_drawbox(x, y, w, h) {
	ctx.strokeStyle = col_grat_border;
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.stroke();
}

function grat_drawf(x, y, w, h) {
	var fscale = Math.log10(frange) / w;
	/* draw some numbers */
	ctx.font = "12px Arial";
	ctx.textAlign = "center";
	ctx.fillStyle = col_grat_text;
	for (i = 1; i < w * fscale; ++i) {
		ctx.fillText(function (x) {
				if (x >= 1e6)
					return x / 1e6 + "MHz";
				if (x >= 1e3)
					return x / 1e3 + "kHz";
				return x + "Hz";
			} (Math.pow(10, i)), x + i / fscale, y + h + 12);
	}
	/* set clip */
	ctx.save();
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.clip();
	/* draw freq graticule */
	for (i = 1; i < w * fscale + 1; ++i) {
		var xo = i / fscale;
		ctx.strokeStyle = col_grat_dark;
		ctx.beginPath();
		ctx.moveTo(x + xo, y);
		ctx.lineTo(x + xo, y + h);
		ctx.stroke();
		ctx.strokeStyle = col_grat_light;
		ctx.beginPath();
		for (j = 2; j < 10; ++j) {
			xo = (i - 1) / fscale + Math.log10(j) / fscale;
			ctx.moveTo(x + xo, y);
			ctx.lineTo(x + xo, y + h);
		}
		ctx.stroke();
	}
	/* no mo clip */
	ctx.restore();
}

function grat_drawlin(x, y, w, h, range, offset, ts) {
	var gscale = h / range;
	ctx.font = "12px Arial";
	ctx.textAlign = "right";
	ctx.fillStyle = col_grat_text;
	ctx.strokeStyle = col_grat_light;
	for (i = ts; i < range; i += ts) {
		ctx.beginPath();
		ctx.moveTo(x, y + i * gscale);
		ctx.lineTo(x + w, y + i * gscale);
		ctx.fillText(-(i - offset), x - 2, y + i * gscale + 4);
		if (i - offset === 0) {
			ctx.strokeStyle = col_grat_dark;
			ctx.stroke();
			ctx.strokeStyle = col_grat_light;
		}
		ctx.stroke();
	}
	ctx.fillText(offset, x - 2, y + 4);
	ctx.fillText(offset - range, x - 2, y + h + 4);
}

function plot_gain(x, y, w, h, f) {
	var gscale = h / grange;
	var fscale = Math.log10(frange) / w;
	var l = 0, ret = -1;
	/* set clip */
	ctx.save();
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.clip();
	/* graph */
	ctx.beginPath();
	ctx.moveTo(x, y + gstart * gscale - Math.log10(f(Math.pow(10, 0 * fscale) * 2 * Math.PI)) * gscale * 20);
	for (i = 1; i < w; ++i) {
		var n = f(Math.pow(10, i * fscale) * 2 * Math.PI);
		ctx.lineTo(x + i, y + gstart * gscale -  Math.log10(n) * gscale * 20);
		if (l > 1 && n <= 1)
			ret = Math.pow(10, i * fscale) * 2 * Math.PI;
		l = n;
	}
	ctx.stroke();
	/* no mo clip */
	ctx.restore();
	return ret;
}

function plot_phase(x, y, w, h, f) {
	var pscale = h / prange;
	var fscale = Math.log10(frange) / w;
	/* set clip */
	ctx.save();
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.clip();
	/* graph */
	ctx.beginPath();
	ctx.moveTo(x, y + pstart * pscale - f(Math.pow(10, 0 * fscale) * 2 * Math.PI) / Math.PI * 180 * pscale);
	for (i = 1; i < w; ++i)
		ctx.lineTo(x + i, y + pstart * pscale - f(Math.pow(10, i * fscale) * 2 * Math.PI) / Math.PI * 180 * pscale);
	ctx.stroke();
	/* no mo clip */
	ctx.restore();
}

var m_top = 20;
var m_left = 30;
var m_bottom = 20;
var m_right = 10;
var m_legend = 30;

var gx, gy, gw, gh; /* gain graph dimensions */
var px, py, pw, ph; /* phase graph dimensions */

function grat_init() { /* draws the text and graticules */
	/* update dimensions */
	var xl = c.height - m_legend;
	var hl = c.height - xl;
	gx = m_left;
	gy = m_top;
	gw = c.width - m_left - m_right;
	gh = xl / 2 - m_top - m_bottom;
	px = m_left;
	py = xl / 2 + m_top;
	pw = c.width - m_left - m_right;
	ph = xl / 2 - m_top - m_bottom;
	/* get to it */
	ctx.font = "16px Arial";
	ctx.textAlign = "center";
	ctx.fillStyle = col_grat_title;
	ctx.fillText("Gain", c.width / 2, 15);
	ctx.fillText("Phase", c.width / 2, xl / 2 + 15);
	grat_drawbox(gx, gy, gw, gh);
	grat_drawbox(px, py, pw, ph);
	grat_drawf(gx, gy, gw, gh);
	grat_drawf(px, py, pw, ph);
	grat_drawlin(gx, gy, gw, gh, grange, gstart, gstep);
	grat_drawlin(px, py, pw, ph, prange, pstart, pstep);
	/* indicate 45 degree phase margin */
	var pscale = ph / prange;
	ctx.strokeStyle = col_pmargin;
	ctx.beginPath();
	ctx.moveTo(px, py + pstart * pscale + 135 * pscale);
	ctx.lineTo(px + pw, py + pstart * pscale + 135 * pscale);
	ctx.stroke();
	/* draw legend */
	var lw = 20;
	var to = m_left;
	ctx.font = "12px Arial";
	ctx.textAlign = "left";
	ctx.fillStyle = col_grat_title;
	
	function addtext(t) {
		ctx.fillText(t, to + lw + 5, xl + hl / 2 + 6);
		to += 20 + lw + 10 + ctx.measureText(t).width;
	}
	ctx.strokeStyle = col_filter;
	ctx.beginPath();
	ctx.moveTo(to, xl + hl / 2);
	ctx.lineTo(to + lw, xl + hl / 2);
	ctx.stroke();
	addtext("Filter & modulator");
	ctx.strokeStyle = col_comp;
	ctx.beginPath();
	ctx.moveTo(to, xl + hl / 2);
	ctx.lineTo(to + lw, xl + hl / 2);
	ctx.stroke();
	addtext("Compensator");
	ctx.strokeStyle = col_total;
	ctx.beginPath();
	ctx.moveTo(to, xl + hl / 2);
	ctx.lineTo(to + lw, xl + hl / 2);
	ctx.stroke();
	addtext("Total");
}

function mark_freq(f, c) {
	var fscale = Math.log10(frange) / gw;
	/* mark resonant freq */
	ctx.strokeStyle = c;
	ctx.beginPath();
	ctx.moveTo(gx + Math.log10(f / (2 * Math.PI)) / fscale, gy);
	ctx.lineTo(gx + Math.log10(f / (2 * Math.PI)) / fscale, gy + gh);
	ctx.moveTo(px + Math.log10(f / (2 * Math.PI)) / fscale, py);
	ctx.lineTo(px + Math.log10(f / (2 * Math.PI)) / fscale, py + ph);
	ctx.stroke();
}

/****************************************************************************/

/* power stage parameters */
var C, ESR, L, DCR;
var PWM_RANGE; // PWM 0-100% voltage range
var VIN; // voltage across output stage bridge
var FSW; // switching frequency

/* compensator parameters */
var C1 = 6.8e-9;
var R1 = 150;
var Rf1 = 4.12e3;
var Rf2 = 10;
var C2 = 2.7e-9;
var R2 = 20.5e3;
var C3 = 0.22e-9;

function mag(x, y) {
	return Math.sqrt(x * x + y * y);
}

function phase(x, y) {
	return Math.atan2(y, x);
}

function gain_filter_and_modulator(x) {
	var xl = x * L;
	var xc = - 1 / (x * C);
	return (VIN / PWM_RANGE) * mag(ESR, xc) / mag(DCR + ESR, xl + xc);
}

function phase_filter(x) {
	var xl = x * L;
	var xc = - 1 / (x * C);
	return (phase(ESR, xc) - phase(DCR + ESR, xl + xc));
}

function compensator(x) {
	var zin = crec(cadd(1 / Rf1, crec({ re: R1, im: -1 / (x * C1) })));
	var zfb = crec(cadd(crec({ re: 0, im: -1 / (x * C3) }), crec({ re: R2, im: -1 / (x * C2) })));
	var gain = cdiv(zfb, zin);
	return gain;
}

function gain_compensator(x) {
	return cmag(compensator(x));
}

function phase_compensator(x) {
	return cphase(compensator(x));
}

function gain_total(x) {
	return gain_filter_and_modulator(x) * gain_compensator(x);
}

function phase_total(x) {
	return phase_filter(x) + phase_compensator(x);
}

function findvalues() {
	var fres = 1 / Math.sqrt(L * C) / (2 * Math.PI);
	var fesr = 1 / (C * ESR) / (2 * Math.PI);
	
	R2 = TBW / fres * (PWM_RANGE / VIN) * Rf1;
	C2 = 1 / (Math.PI * R2 * fres);
	C3 = C2 / (2 * Math.PI * R2 * C2 * fesr - 1);
	R1 = Rf1 / (FSW / (fres * 2) - 1);
	C1 = 1 / (Math.PI * R1 * FSW);

	if (C3 == 0) C3 = 1e-21; // in case of 0 ESR
	
	R1 = R1.toPrecision(6);
	R2 = R2.toPrecision(6);
	C1 = C1.toPrecision(6);
	C2 = C2.toPrecision(6);
	C3 = C3.toPrecision(6);
	
	document.getElementById('valR1').value = R1;
	document.getElementById('valR2').value = R2;
	document.getElementById('valC1').value = (C1 / 1e-9).toPrecision(6);
	document.getElementById('valC2').value = (C2 / 1e-9).toPrecision(6);
	document.getElementById('valC3').value = (C3 / 1e-9).toPrecision(6);
}

function redraw() {
	/* get values from page */
	C = document.getElementById('valC').value * 1e-6;
	ESR = document.getElementById('valE').value * 1e-3;
	L = document.getElementById('valL').value * 1e-6;
	DCR = document.getElementById('valD').value * 1e-3;
	PWM_RANGE = document.getElementById('valI').value;
	VIN = document.getElementById('valV').value;
	FSW = document.getElementById('valF').value * 1e3;
	Rf1 = document.getElementById('valRf1').value;
	TBW = document.getElementById('valT').value * 1e3;

	/* make a guess */
	if (document.getElementById("autofill").checked == true)
		findvalues();
	else {
		R1 = document.getElementById('valR1').value;
		R2 = document.getElementById('valR2').value;
		C1 = document.getElementById('valC1').value * 1e-9;
		C2 = document.getElementById('valC2').value * 1e-9;
		C3 = document.getElementById('valC3').value * 1e-9;
	}
	/* start */
	ctx.clearRect(0, 0, c.width, c.height);
	grat_init(); /* draw the graticules etc */
	mark_freq(1 / Math.sqrt(L * C), col_fres); /* mark resonant freq */
	mark_freq(1 / (C * ESR), col_fesr); /* mark fESR */
	/* filter and modulator */
	ctx.strokeStyle = col_filter;
	plot_gain(gx, gy, gw, gh, gain_filter_and_modulator);
	plot_phase(px, py, pw, ph, phase_filter);
	/* compensator */
	ctx.strokeStyle = col_comp;
	plot_gain(gx, gy, gw, gh, gain_compensator);
	plot_phase(px, py, pw, ph, phase_compensator);
	/* total */
	ctx.strokeStyle = col_total;
	var fc = plot_gain(gx, gy, gw, gh, gain_total);
	plot_phase(px, py, pw, ph, phase_total);
	/* mark 0db crossing */
	mark_freq(fc, col_fc);
	/* collect some info */
	var fres = 1 / Math.sqrt(L * C) / (2 * Math.PI);
	var fesr = 1 / (C * ESR) / (2 * Math.PI);
	var info = "FLC = " + fres.toPrecision(6) + "\nFESR = " + fesr.toPrecision(6) + "\nBW = " + (fc / (2 * Math.PI)).toPrecision(6);
	document.getElementById('info').innerHTML = info;
}

$(function() {
	redraw();
});
